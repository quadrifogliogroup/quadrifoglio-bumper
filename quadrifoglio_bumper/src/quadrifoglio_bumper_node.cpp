#include "ros/ros.h"
#include <pcl_ros/point_cloud.h>
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/PointCloud2.h"

#define TODEG 57.29577951 //Conversion factors for degrees & radians
#define TORAD 0.017453293 //The AVR cannot reach this level of precision but w/e

class VirtualBumper
{
  public:
    VirtualBumper(ros::Publisher *cmdVelOutPub, geometry_msgs::Twist *cmdVelOutMsg, ros::Publisher *debugCloudPub)
    {
        _cmdVelOutPub = cmdVelOutPub;
        _cmdVelOutMsg = cmdVelOutMsg;
        _debugCloudPub = debugCloudPub;
        _gotFirst = false; //until first actual pcl arrives
    }
    void cmdVelInCallback(const geometry_msgs::Twist::ConstPtr &twistMsg)
    {
        if (_gotFirst)//Check pointcloud validity
        {                               
            *_cmdVelOutMsg = *twistMsg; 
            if(_frontBlocked && _rearBlocked){
                _cmdVelOutMsg->linear.x = 0.0;
            }
            else{
                _cmdVelOutMsg->linear.x += _counterThrottle;
            }
/*            if (_frontBlocked && _cmdVelOutMsg->linear.x > 0.0)
            {
                _cmdVelOutMsg->linear.x = ;
            }
            if (_rearBlocked && _cmdVelOutMsg->linear.x < 0.0)
            {
                _cmdVelOutMsg->linear.x = 0.0;
            }*/

        }
        else
        {
            _cmdVelOutMsg->linear.x = 0;
            _cmdVelOutMsg->linear.y = 0;
            _cmdVelOutMsg->linear.z = 0;
            _cmdVelOutMsg->angular.x = 0;
            _cmdVelOutMsg->angular.y = 0;
            _cmdVelOutMsg->angular.z = 0;
        }
        _cmdVelOutPub->publish(*_cmdVelOutMsg);
    }
    void pclCallback(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr &pclMsg)
    {
        //_pcl = *pclMsg;
        _gotFirst = true;

        _debugCloudMsg.header = pclMsg->header;
        _debugCloudMsg.points.clear();
        long sizeCounter = 0;
        int frontBinHits = 0;
        int rearBinHits = 0;
        float lowest = 999;
        float highest = -999;
        for (auto point : pclMsg->points)
        { //negative x is forward, negative y is left wrt rplidar
            
            if (point.x < 0.75 && point.x > 0.0 && fabs(point.y) < 0.25)
            { //Rear bumper bin
                if(point.x < lowest) lowest = point.x;
                _debugCloudMsg.points.push_back(point);
                sizeCounter++;
                rearBinHits++;
            }
            if (point.x > -0.45 && point.x < 0.0 && fabs(point.y) < 0.25)
            { //Front bumper bin
                if(point.x > highest) highest = point.x;
                _debugCloudMsg.points.push_back(point);
                sizeCounter++;
                frontBinHits++;
            }
        }
        _frontBlocked = (frontBinHits) ? true : false;
        _rearBlocked = (rearBinHits) ? true : false;
        _counterThrottle = 0.0;

        if(lowest < 0.75){ //If too close to something in the rear
            _counterThrottle += (0.75-lowest)*2;
        }
        if(highest > -0.45){ //If too close to something in the front
            _counterThrottle -= (0.45+highest)*2;
        }

        //        ROS_INFO("Front bumper hits: %d Rear bumper hits: %d", frontBinHits, rearBinHits);
        ROS_INFO("Front bumper blocked: %d Rear bumper blocked: %d", _frontBlocked, _rearBlocked);
        _debugCloudMsg.width = sizeCounter;
        _debugCloudMsg.height = 1;
        _debugCloudPub->publish(_debugCloudMsg);
    }

  private:
    ros::Publisher *_cmdVelOutPub;
    ros::Publisher *_debugCloudPub;
    geometry_msgs::Twist *_cmdVelOutMsg;
    pcl::PointCloud<pcl::PointXYZ> _debugCloudMsg;
    bool _gotFirst;
    bool _frontBlocked = false;
    bool _rearBlocked = false;
    float _counterThrottle = 0.0;
};

int main(int argc, char **argv)
{

    ros::init(argc, argv, "quadrifoglio_bumper");
    ros::NodeHandle n;

    ros::Publisher cmdVelOutPub = n.advertise<geometry_msgs::Twist>("cmd_vel", 1);
    ros::Publisher debugCloudPub = n.advertise<pcl::PointCloud<pcl::PointXYZ>>("debug_cloud", 1);
    geometry_msgs::Twist cmdVelOutMsg;

    VirtualBumper virtualBumper(&cmdVelOutPub, &cmdVelOutMsg, &debugCloudPub);

    ros::Subscriber cmdVelInSub = n.subscribe("cmd_vel_raw", 1, &VirtualBumper::cmdVelInCallback, &virtualBumper);
    ros::Subscriber pclSub = n.subscribe("pointcloud", 1, &VirtualBumper::pclCallback, &virtualBumper);

    ros::spin();

    return 0;
}
